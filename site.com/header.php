<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <title>Hello, world!</title>
    </head>
    <body>

        <div class="container">

            <header>
                <ul class="nav nav-pills mt-4">
                    <li class="nav-item">
                        <a class="nav-link <?php
                        if ($_SERVER['SCRIPT_NAME']=="/index.php") {
                            echo "active";
                        }
                        ?>" href="index.php">Главная</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php
                        if ($_SERVER['SCRIPT_NAME']=="/products.php") {
                            echo "active";
                        }
                        ?>" href="products.php">Продукция</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php
                        if ($_SERVER['SCRIPT_NAME']=="/contacts.php") {
                            echo "active";
                        }
                        ?>" href="contacts.php">Контакты</a>
                    </li>
                </ul>

                <div class="jumbotron jumbotron-fluid mt-4">
                    <div class="container">
                        <h1 class="display-4">Fluid jumbotron</h1>
                        <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
                    </div>
                </div>
            </header>
             <main>
