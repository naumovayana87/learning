<?php ?>
<!doctype html>
<html lang = "en">
    <head>
        <!--Required meta tags -->
        <meta charset = "utf-8">
        <meta name = "viewport" content = "width=device-width, initial-scale=1, shrink-to-fit=no">
        <script src = "https://kit.fontawesome.com/3b6ad43031.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="./main.css"/>

        <title>Hello, world!</title>
        <style>
            input:valid{
                border-color: green;
            }
            input:invalid{
                border-color: red;
            }
            input:valid+small+small{
                display:none;
            }
            input:invalid+small{
                display:none;
            }
        </style>
    </head>
    <body>
        <main>
            <?php include 'header.php' ?>;
            <div class="container">
                <div class="row">

                    <div class="creditCardForm">
                        <div class="payment">
                            <form>
                                <div class="form-group" id="card-number-field">
                                    <label for="cardNumber">Card Number</label>
                                    <input type="text" required pattern="^([0-9]{4}\s[0-9]{4}\s[0-9]{4}\s[0-9]{4})$" class="form-control" id="cardNumber">
                                    <small class="form-text text-success">
                                        <i class="fas fa-check "></i>
                                        Всё корректно!
                                    </small>
                                    <small class="form-text text-muted">
                                        *Обязательно к заполнению
                                    </small>
                                </div>

                                <div class="form-group owner">
                                    <label for="owner">First Name</label>
                                    <input type="text" required pattern="^([A-Za-z]{2,})$" class="form-control" id="owner">
                                    <small class="form-text text-success">
                                        <i class="fas fa-check "></i>
                                        Всё корректно!
                                    </small>
                                    <small class="form-text text-muted">
                                        *Обязательно к заполнению
                                    </small>
                                    <label for="owner">Last Name</label>
                                    <input type="text" class="form-control" required pattern="^([A-Za-z]{2,})$" id="owner">
                                    <small class="form-text text-success">
                                        <i class="fas fa-check "></i>
                                        Всё корректно!
                                    </small>
                                    <small class="form-text text-muted">
                                        *Обязательно к заполнению
                                    </small>
                                </div>
                                <div class="form-group CVV">
                                    <label for="cvv">CVV</label>
                                    <input type="text" class="form-control" required pattern="^([0-9]{3})$" id="cvv">
                                    <small class="form-text text-success">
                                        <i class="fas fa-check "></i>
                                        Всё корректно!
                                    </small>
                                    <small class="form-text text-muted">
                                        *Обязательно к заполнению
                                    </small>
                                </div>
                                <div class="form-group" id="expiration-date">
                                    <label>Expiration Date</label>
                                    <select>
                                        <option value="01">January</option>
                                        <option value="02">February </option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                    <select>
                                        <option value="16"> 2016</option>
                                        <option value="17"> 2017</option>
                                        <option value="18"> 2018</option>
                                        <option value="19"> 2019</option>
                                        <option value="20"> 2020</option>
                                        <option value="21"> 2021</option>
                                    </select>
                                </div>
                                <div class="form-group" id="credit_cards">
                                    <img src="https://bootstraptema.ru/snippets/form/2017/visa.jpg" id="visa">
                                    <img src="https://bootstraptema.ru/snippets/form/2017/mastercard.jpg" id="mastercard">
                                    <img src="https://bootstraptema.ru/snippets/form/2017/amex.jpg" id="amex">
                                </div>
                                <div class="form-group" id="pay-now">
                                    <button type="submit" class="btn btn-default" id="confirm-purchase">Confirm</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php include 'list.php' ?>
                <?php include 'comments.php' ?>
            </div>
            <?php include 'footer.php' ?>
        </main>
    </body>
</html>