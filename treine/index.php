<?php ?>
<!doctype html>
<html lang = "en">
    <head>
        <!--Required meta tags -->
        <meta charset = "utf-8">
        <meta name = "viewport" content = "width=device-width, initial-scale=1, shrink-to-fit=no">
        <script src = "https://kit.fontawesome.com/3b6ad43031.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="./main.css"/>
        <title>Hello, world!</title>
        <script src = "main.js"></script>
    </head>
    <body onload="onLoad();">
        <main>
            <?php include 'header.php' ?>;
            <div class="container">
                <div class="card">
                    <div class="row">
                        <div class="col-sm">
                            <div class="card-body text-center">
                                <h5 class="card-title" style="color:white">LIMITED SUMMER OFFER</h5>
                                 <p class="time text-white"></p>
                                <img src="img/2.jpg" class="card-img-top my_img2 mt-2" alt="picter">
                                <p><b>Trial</b></p>
                                <p class="card-text">The first week only for</p>
                                <h5>$0.99</h5>
                                <p class="card-text">Rebils at $8.99 per month after your<br>
                                    trial is completed.</p>
                                </p>
                                <a href="pay.php" class="btn">Get started</a>
                            </div>
                        </div>
                        <div class="col-sm">
                            <div class="card-body text-center">
                                <h5 class="card-title" style="color:white">LIMITED SUMMER OFFER</h5>
                                <img src="img/1.jpg" class="card-img-top my_img2" alt="picter">
                                <p style="margin-bottom: 3.5rem;"><b>Annual Plan</b></p>
                                <h5>$39.00</h5>
                                <p class="card-text">Your Premium plan with auto-renew.<br>
                                    Cancel any time</p>
                                </p>
                                <a href="pay.php" class="btn">Get started</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php include 'list.php' ?>
                <?php include 'comments.php' ?>
            </div>
            <?php include 'footer.php' ?>
        </main>
    </body>
</html>