"use strict";
function onLoad() {
    showTime();
    setInterval(() => {
        showTime();
    }, 1000 * 60);
}
function showTime() {
    let seconds = (Date.parse("12/30/2019") - new Date()) / 1000;
    let days = parseInt(seconds / 60 / 60 /  24);
    seconds = seconds - days * 24 * 60 * 60;
    let hours = parseInt(seconds / 60 / 60);
    seconds = seconds - hours * 60 * 60;    
    let minutes = parseInt(seconds / 60);
    document.querySelector('.time').innerText = days + "d" + ":" + hours +"h"+":" + minutes+"m";
}
