<?php ?>
<header>
    <nav class="navbar navbar-expand shadow p-3 mb-5 bg-white rounded fixed-top">
        <img class="navbar-brand my_img3" style="width:30px; margin-left:55px; margin-right:5px;" src="img/logo.png" alt="logo">
        <a class="navbar-brand text-success font-weight-bold" href="index.php">MY LETTER</a>
        <a class="nav-link text-success ml-auto font-weight-bold my_a2" style="margin-right:40px;" href="#">Edit details in your cover letter</a>
    </nav>
    <h1>Your profile is ready. Get your personalized cover letter now for Less<br>
        than the price of a cup of coffee and improve your career</h1>
</header>
