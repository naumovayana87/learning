<!doctype html>
<!--
Есть сумма депозита, годовая процентная
ставка, и срок размещения вклада в месяцах.
Необходимо рассчитать сколько денег по
окончанию получит вкладчик (сумма вклада +
проценты). Считать, что расчёт процентов
выполняется помесячно без влияния количества
дней в месяце, также не учитывать налоги,
капитализацию учитываем по желанию.
-->
<html lang="ru">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <title>Депозитный калькулятор для вкладчиков</title>
        <?php

        function deposit() {
            $sum = $_REQUEST["sum_input"];
            $sum2 = $sum;
            $rate = $_REQUEST["rate_input"];
            $term = $_REQUEST["term_input"];
            $cap_input = $_GET['cap_input'];
            if ($cap_input==1){
                for ($i = 1; $i <= $term; $i++) {
                $percents = $sum * $rate / 12 / 100;
                $sum = $sum + $percents;
            }
            $sum=floor($sum * 100) / 100;
            return $sum;
            }else{
                $procent2 = $sum * $term * $rate / 12 / 100;
            $summa2 = $sum2 + $procent2;
            $summa2=floor($summa2 * 100) / 100;
            return $summa2;
            }       
        }
        ?>
    </head>
    <body>
        <div class="container mt-5">

            <h1 class="mb-4">Депозитный калькулятор для вкладчиков</h1>
            <form method="GET">
                <!-- Input Data Form -->
                <div class="form mb-5">	
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="sum_input">Сумма вклада (грн.)</label>
                            <input type="number" class="form-control" name="sum_input" id="sum_input" 	min="1000" max="999999999" value="<?=$_GET["sum_input"]?>">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="term_input">Срок вклада (мес.)</label>
                            <input type="number" class="form-control" name="term_input" id="term_input" min="3" max="48" value="<?=$_GET["term_input"]?>">
                        </div> 
                        <div class="form-group col-md-4">
                            <label for="rate_input">Ставка (% годовых)</label>
                            <input type="number" class="form-control" name="rate_input" id="rate_input" min="1" max="100" value="<?=$_GET["rate_input"]?>" step='0.01'>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="cap_input">Капитализация %</label>
                        <input type="checkbox" class="form-control" name="cap_input" value="1">
                    </div>
                    <button type="submit" class="btn btn-primary w-100">Рассчитать</button>
                </div>

            </form>
            <!-- /Input Data Form -->

            <?php
            echo "<h3>По завершению депозита вы получите: " . deposit() . " грн.</h3>";
            ?>
        </div>
    </body>
</html>