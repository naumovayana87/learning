<?php include ("header.php"); ?>
<?php include ("jumbotron.php"); ?>
<div class="card">
    <img src="https://images.unsplash.com/photo-1564324738191-7f91304232eb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80" class="card-img-top" style="height:340px;" alt="...">
    <div class="card-body">
        <h1 class="card-title">Lorem ipsum dolor sit amet, consectetur adipiscing</h1>
        <p class="text-uppercase text-secondary">September 8, 2016 | music</p>
        <hr class="ml-0" style="width:130px;">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eu eros elit. Nulla id consequat felis. Mauris imperdiet ipsum ac lacinia accumsan. Sed iaculis tristique posuere. Duis eleifend tortor venenatis rhoncus condimentum.</p>
        <h3>Lorem ipsum dolor sit amet, consectetur</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eu eros elit. Nulla id consequat felis. Mauris imperdiet ipsum ac lacinia accumsan. Sed iaculis tristique posuere. Duis eleifend tortor venenatis rhoncus condimentum</p>
        <ul>
            <li>Lorem ipsum dolor</li>
            <li>sit amet, consectetur</li>
            <li>adipiscing elit. Morbi</li>
        </ul>
        <p>Lorem ipsum dolor sit amet, <a href="#" style="text-decoration:underline;">consectetur adipiscing elit.</a> Cras et arcu mi. Nulla tincidunt ligula mi, eget gravida ante scelerisque quis. Mauris ut ante elit. Praesent fringilla rutrum nisi, sed mattis dui ultricies in. Phasellus mattis, turpis at fringilla aliquam, sapien sapien efficitur libero, nec aliquet dolor purus non nisi. Quisque eros massa, fringilla ut libero vel, dictum dignissim sapien. Proin augue est, mattis ultricies neque quis, pellentesque imperdiet urna.</p>
        <p>Aenean rutrum congue nisl eu ultrices. In rhoncus metus urna, nec condimentum tortor sagittis a. Nulla a ligula ultricies, vehicula dui quis, ultrices dui. Proin efficitur vestibulum aliquam. In id ipsum est. Nunc pellentesque facilisis diam in ornare. In bibendum ornare felis, non euismod est convallis sollicitudin. Quisque porta dignissim neque ac bibendum. Nullam imperdiet ac dolor ut condimentum. Maecenas eget vehicula nisi, eu suscipit quam. Donec finibus, mi in lacinia placerat, quam quam aliquam ex, eget iaculis nunc leo consectetur sem. Mauris sed ipsum efficitur, molestie dolor eget, euismod ante. Suspendisse vel justo faucibus, tristique mauris a, scelerisque tellus. Morbi lacinia ultricies lacinia. In sed quam sed arcu lobortis placerat ut volutpat orci. Phasellus vitae gravida libero.</p>
        <p>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Etiam lacinia convallis sodales. In scelerisque, dui nec consectetur venenatis, lacus justo finibus diam, at blandit augue dui molestie nulla. Sed euismod tellus vitae tortor laoreet, vitae lacinia nunc posuere. Quisque at porttitor nisi. Donec vel mauris non felis consequat interdum a in ligula. Maecenas porttitor massa nec massa congue venenatis. Cras maximus tellus elementum, maximus diam ac, tempus quam. Nulla at mollis mi.</p>
    </div>
</div>
<?php include ("footer.php"); ?>