c_button.onclick = function () {
    if (c_button.classList.contains("selected")) {
        return;
    }
    c_button.classList.add("selected");
    f_button.classList.remove("selected");
    k_button.classList.remove("selected");

    let tag = document.querySelectorAll("h6");
    for (let i = 0; i < tag.length; i++) {
        let data = tag[i].innerHTML;
        if (data.indexOf('F') == -1) {
            data = parseInt(data);
            data = data - 273.15;
            data = Math.round(data);
            tag[i].innerHTML = data + " C";
        } else {
            data = parseInt(data);
            data = (data - 32) / 1.8;
            data = Math.round(data);
            tag[i].innerHTML = data + " C";
        }
    }
}

f_button.onclick = function () {
    if (f_button.classList.contains("selected")) {
        return;
    }
    f_button.classList.add("selected");
    c_button.classList.remove("selected");
    k_button.classList.remove("selected");

    let tag = document.querySelectorAll("h6");
    for (let i = 0; i < tag.length; i++) {
        let data = tag[i].innerHTML;
        if (data.indexOf('C') == -1) {
            data = parseInt(data);
            data = (data - 273.15) * 9 / 5 + 32;
            data = Math.round(data);
            tag[i].innerHTML = data + " F";
        } else {
            data = parseInt(data);
            data = data * 1.8 + 32;
            data = Math.round(data);
            tag[i].innerHTML = data + " F";
        }
    }
}
k_button.onclick = function () {
    if (k_button.classList.contains("selected")) {
        return;
    }
    k_button.classList.add("selected");
    c_button.classList.remove("selected");
    f_button.classList.remove("selected");

    let tag = document.querySelectorAll("h6");
    for (let i = 0; i < tag.length; i++) {
        let data = tag[i].innerHTML;
        if (data.indexOf('C') == -1) {
            data = parseInt(data);
            data = (data - 32) * 5 / 9 + 273.15;
            data = Math.round(data);
            tag[i].innerHTML = data + " K";
        } else {
            data = parseInt(data);
            data = data + 273.15;
            data = Math.round(data);
            tag[i].innerHTML = data + " K";
        }
    }
}

