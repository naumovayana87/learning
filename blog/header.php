<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"/>
        <style>
            .card{
                grid-area:card;
                display:grid;
                grid-template-columns:1fr;
                grid-gap:20px;
                grid-template-rows:auto;
                border:none;
            }
            .jumbotron{
                grid-area:jumbotron;
                border-radius:5px;
                padding-top: 2rem;
            }
            .container{
                min-height: calc(100vh - 56px);
                display:grid;
                grid-gap:20px;
                grid-template-columns:1fr 2fr;
                grid-template-rows:auto;
                grid-template-areas:"jumbotron card"
                    ". card"
            }
            .row{
                border-radius: 3px;
                margin:0px;
            }
            .my_col{
                margin:auto;
            }
            .my_img{
                width:160px;
                height:160px;
                border-radius:90px;
            }
            img{
                height:150px;
                width:200px;
            }
            .column{
                padding:20px;
                margin:10px;
            }
            .my_btn{
                border: 1px solid grey;
                padding:5px;
                border-radius:3px;
            }
            .stretched-link {
                white-space: nowrap;
                text-overflow: ellipsis;
                overflow: hidden;
                width: 87px;
                vertical-align: top;
                display: inline-block;
            }
        </style>
    </head>
    <body>
        <main>
            <header>
                <div class="navabar bg-light mb-4">
                    <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                                <li class="nav-item">
                                    <a class="nav-link <?php
                                    if ($_SERVER['SCRIPT_NAME'] == "/index.php") {
                                        echo "active";
                                    }
                                    ?>" href="index.php">Home</a>
                                </li>
                                </li
                                <li class="nav-item">
                                    <a class="nav-link <?php
                                    if ($_SERVER['SCRIPT_NAME'] == "/contents.php") {
                                        echo "active";
                                    }
                                    ?>"href="contents.php">Contents</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?php
                                       if ($_SERVER['SCRIPT_NAME'] == "/coments.php") {
                                       echo "active";
                                       }
                                       ?>"href="coments.php">Coments</a>
                                </li>
                        </div>
                    </nav>
                </div>
            </header>
            <div class="container">
