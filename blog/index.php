<?php include ("header.php"); ?>
<?php include ("jumbotron.php"); ?>
                <div class="card">
                    <div>
                        <div class="row no-gutters bg-light position-relative">
                            <div class="column">
                                <img src="https://images.unsplash.com/photo-1556820688-fc6ac66691ca?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=632&q=80" alt="...">
                            </div>
                            <div class="col-md-6 position-static pt-4 pl-md-0">
                                <h5 class="mt-0">Lorem ipsum dolor sit amet, consectetur</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at mollis ipsum. Donec vel vehicula velit, vitae suscipit quam. <a href="#" class="stretched-link text-body"><i> Read more about</i></a></p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row no-gutters bg-light position-relative">
                            <div class="column">
                                <img src="https://images.unsplash.com/photo-1508403812187-25375c49467b?ixlib=rb-1.2.1&auto=format&fit=crop&w=967&q=80" alt="...">
                            </div>
                            <div class="col-md-6 position-static pt-4 pl-md-0">
                                <h5 class="mt-0">Lorem ipsum dolor sit amet, consectetur</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at mollis ipsum. Donec vel vehicula velit, vitae suscipit quam. <a href="#" class="stretched-link text-body"><i> Read more about</i></a></p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row no-gutters bg-light position-relative">
                            <div class="column">
                                <img src="https://images.unsplash.com/photo-1517790253044-a9968449de15?ixlib=rb-1.2.1&auto=format&fit=crop&w=1064&q=80" alt="...">
                            </div>
                            <div class="col-md-6 position-static pt-4 pl-md-0">
                                <h5 class="mt-0">Lorem ipsum dolor sit amet, consectetur</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at mollis ipsum. Donec vel vehicula velit, vitae suscipit quam. <a href="#" class="stretched-link text-body"><i> Read more about</i></a></p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row no-gutters bg-light position-relative">
                            <div class="column">
                                <img src="https://images.unsplash.com/photo-1523459871272-27bb6e3e0486?ixlib=rb-1.2.1&auto=format&fit=crop&w=1053&q=80" alt="...">
                            </div>
                            <div class="col-md-6 position-static pt-4 pl-md-0">
                                <h5 class="mt-0">Lorem ipsum dolor sit amet, consectetur</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at mollis ipsum. Donec vel vehicula velit, vitae suscipit quam. <a href="#" class="stretched-link text-body"><i> Read more about</i></a></p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row no-gutters bg-light position-relative">
                            <div class="column">
                                <img src="https://images.unsplash.com/photo-1533743914085-403451366d53?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80" alt="...">
                            </div>
                            <div class="col-md-6 position-static pt-4 pl-md-0">
                                <h5 class="mt-0">Lorem ipsum dolor sit amet, consectetur</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at mollis ipsum. Donec vel vehicula velit, vitae suscipit quam. <a href="#" class="stretched-link text-body"><i> Read more about</i></a></p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row no-gutters bg-light position-relative">
                            <div class="column">
                                <img src="https://images.unsplash.com/photo-1547182131-01ef6da636e5?ixlib=rb-1.2.1&auto=format&fit=crop&w=723&q=80" alt="...">
                            </div>
                            <div class="col-md-6 position-static pt-4 pl-md-0">
                                <h5 class="mt-0">Lorem ipsum dolor sit amet, consectetur</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at mollis ipsum. Donec vel vehicula velit, vitae suscipit quam. <a href="#" class="stretched-link text-body"><i> Read more about</i></a></p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row no-gutters bg-light position-relative">
                            <div class="column">
                                <img src="https://images.unsplash.com/photo-1470298961567-3c24e6d6a086?ixlib=rb-1.2.1&auto=format&fit=crop&w=1051&q=80https://images.unsplash.com/photo-1547182131-01ef6da636e5?ixlib=rb-1.2.1&auto=format&fit=crop&w=723&q=80" alt="...">
                            </div>
                            <div class="col-md-6 position-static pt-4 pl-md-0">
                                <h5 class="mt-0">Lorem ipsum dolor sit amet, consectetur</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at mollis ipsum. Donec vel vehicula velit, vitae suscipit quam. <a href="#" class="stretched-link text-body"><i> Read more about</i></a></p>
                            </div>
                        </div>
                    </div>
                </div>
<?php include ("footer.php"); ?>