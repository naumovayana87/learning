    <div class="jumbotron bg-light">
        <div class="row">
            <div class="my_col">
                <img class="my_img"  src="https://images.unsplash.com/photo-1504276048855-f3d60e69632f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80" alt="...">
            </div>
            <div class="my_col">
                <h5 class="text-left mt-3">Lorem ipsum dolor sit amet, consecteturadipis cing elit. Praesent consectetur ipsum eu elit eleifend, ac volutpat.</h5>
                <hr class="ml-0" style="width:150px;">
                <h5>Lorem ipsum dolor</h5>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam.</p>
                <p>Donec vel vehicula velit, vitae suscipit qua.</p>
                <hr class="ml-0" style="width:150px;">
                <div class="icon-bar">
                    <a href="#" class="facebook"><i class="fa fa-facebook mr-2"></i></a> 
                    <a href="#" class="google"><i class="fa fa-google mr-2"></i></a> 
                    <a href="#" class="linkedin"><i class="fa fa-linkedin mr-2"></i></a>
                    <a href="#" class="youtube"><i class="fa fa-youtube mr-2"></i></a> 
                    <a href="#"><i class="fa fa-twitter mr-2"></i></a>
                </div>
                <p class="text-uppercase mt-2">Lorem ipsum dolor sit</p>
            </div>
        </div>
    </div>